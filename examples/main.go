package main

import (
	"bitbucket.org/rumbletomato/cla_car"
	"fmt"
)

func main() {
	CLACar := cla_car.NewCLACar(
		"login",
		"token",
		"domain")

	fmt.Println(CLACar.GetArticleBody(1))
}
