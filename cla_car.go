package cla_car

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"text/template"
)

type articleRequestStruct struct {
	DomainName string
	ArticleID  int32
}

type CLACar struct {
	ContentUrlTemplate *template.Template
	DomainName         string
	Login              string
	Token              string
}

type ArticleContent struct {
	Body *ContentBody `json:"body"`
}

type ContentBody struct {
	Storage *BodyStorage `json:"storage"`
}

type BodyStorage struct {
	Value string `json:"value"`
}

func (clacar *CLACar) GetArticleBody(articleID int32) (string, error) {
	requestStruct := articleRequestStruct{
		DomainName: clacar.DomainName,
		ArticleID:  articleID,
	}

	contentUrlBuf := new(bytes.Buffer)
	if err := clacar.ContentUrlTemplate.Execute(contentUrlBuf, requestStruct); err != nil {
		log.Fatal(err)
	}

	contentUrl := contentUrlBuf.String()

	req, err := http.NewRequest("GET", contentUrl, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(clacar.Login, clacar.Token)

	httpClient := new(http.Client)
	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf(resp.Status)
	}

	defer resp.Body.Close()

	var articleContent ArticleContent
	if err := json.NewDecoder(resp.Body).Decode(&articleContent); err != nil {
		log.Fatal(err)
	}

	return articleContent.Body.Storage.Value, nil
}

func NewCLACar(
	login string,
	token string,
	domainName string) *CLACar {

	contentUrlTemplate, err := template.New("content url").
		Parse(`https://{{.DomainName}}.atlassian.net/wiki/rest/api/content/{{.ArticleID}}?expand=body.storage`)
	if err != nil {
		log.Fatal(err)
	}

	return &CLACar{
		Login:              login,
		Token:              token,
		DomainName:         domainName,
		ContentUrlTemplate: contentUrlTemplate,
	}
}
